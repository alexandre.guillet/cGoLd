# Continuous Game of Life with diversity

## Description of the system

This Julia script aims at interacting with the simulation in *d+1*-dimensional space-time $(\vec{x},t)$ of the *continuous Game of Life* with *diversity*:
$$
\begin{align}
\partial_t L_l&=G_l\Big(\sum_l N_{kl}\Big)P_l-L_l \\
N_{kl}&=\phi_k\ast L_l \\
P_l &=\frac{(N_{kl})^\alpha}{\sum_l (N_{kl})^\alpha} \quad , \qquad k=k_P
\end{align}
$$
where $\ast$ is a spatial convolution operation, *Lₗ* is the *Life* field in layer *l*, *Gₗ* is a *k*-variate function, the target or rule for the layer *l*, taking as arguments the sum of *Nₖₗ* *neighbour* fields across layers, each with kernel $\phi_k$. The *weight* fields *Pₗ* selects the region where the rule in layer *l* applies.

Here we use two "neighbour" fields *k=1,2* as in Conway's original *Game of Life* (cell grid state and neighbours count), in Stefan Rafler's *Smooth Life* and in Cornus Ammonis' *Smoother Life*, from which this system derives. The original *Lenia* formulation reduces *k* to 1. Slackermanz' *MNCA* and multi-kernel *Lenia* use larger values of *k*. The multi-channel *Lenia* uses multiple layers to enrich the possibilities. In contrast, multiple layers in this formulation, allow the coexistence of different rules and could be adapted to other models.

The dimensionality of space and the maximum number of layers is changed by editing the `dim` and `nlayer` variables (to any positive integer) in the script.


## Setup for cGoLd_lab.jl

From the Julia REPL, press `]` and install the packages:
```
add GLMakie, Images, FFTW, VideoIO
```

The script relies on the successful installation of the package `GLMakie.jl`, a Julia library that allows to build UI and interact with the simulation. `VideoIO.jl` is used to record the simulation panel only. `Images.jl` is convenient for representing the fields. `FFTW.jl` is the keystone of the simulation.

### Launch

Run the script, close the simulation window (or `CTRL`+`C`) to end it.
After the first run, the script can also be launched from the Julia REPL as a function:
```
cGoLd(dim,nlayers;args...)
```
where `dim=1, 2, 3` or higher and the maximum number of layer `nlayer = 16` can be increased or decreased for performance.
From a terminal (and in the appropriate directory), the script is run as:
```
julia -t8 cGoLd_lab.jl
```
where the optional argument `-tx` sets the number of threads to `x=8`, or the maximum number of available CPU threads.

### Optional arguments

Breakdown of the optional arguments in the `cGoLd` function:

```julia
function cGoLd(dim::Int64, nlayer::Int64; savepath="cGoLd.mkv", framerate=24, compression=28, imsize=Int64[], screensize=(1920,1080), targetsize=(401,401), buffersize=2^10, backgroundcolor=RGBf(0.5, 0.5, 0.5), colourangle=-60, nexc=5)
```

- `savepath`: record title is set without path if not specified at the beginning of the script in `recordtitle`. ########
- `framerate` and `compression` (crf) are ffmpeg's arguments for recording (with codec libx265).
- `imsize` is the size of the simulation frame, such as `[2 1]*2^8`, use powers of 2 for good size-to-speed ratio.
- `screensize` is the size of the figure in pixels when it opens.
- `targetsize` sets the resolution of the top left target image (higher is slower).
- `buffersize` is the size of the (time) array buffer in the bottom left plot.
- `backgroundcolor` sets the background colour of the window.
- `colourangle` sets the starting colour for the first layer: -60 is magenta, 0 is red, 60 is yellow, 120 is green, 180 is cyan, 240 is blue, 300 is magenta again...
- `nexc` sets the spatial downsampling of the simulation image in each direction reported in the top left scatter plot (`nexc=1` means all pixels are plotted).


## Interface

### Target function (top left image)
Superimposed images of the "rules" for each active (non-empty) layer *l*: the target functions $G_l(\sum_l N_{1l},\sum_l N_{2l})$. Scatter plot of the simulation image in this space, to visualize the evaluation at each point in space.
- Pass the mouse on scatter plot to visualize the corresponding pixels in the simulation.
- Scroll to zoom, right-click to pan, with `CTRL`+`X` or `Y` to restrict movement to that direction, and press the `BACKSPACE` key to reset the axes.

### Total mass (left plot)
Total mass in each layer: $\int L_l(\vec{x},t)\textrm{d}^d\vec{x}$.
A layer is considered inactive when its mass is below a certain (small) threshold *m₋*.
The number of active (non-empty) layers, the maximum number of layers `nlayer` and the index of the current layer is displayed.
- Change current layer with `UP`/`DOWN` or `LEFT`/`RIGHT` arrow keys.

### Buttons (bottom left)
- ▶️/⏯️ = PLAY/PAUSE = `SPACE` key.
- ↺ = RESET = `ENTER` key.
- ⬤/◯ = RECORDING/RECORD.
    - Press `CTRL` key when clicking ◯ to record the simulation only (otherwise the full lab is recorded).

### Simulated field (top right image)
- For `dim<=2`: `CTRL` key triggers an isotropic seed at the mouse position, and `SHIFT` key transfer mass from all layers at the mouse position in the current layer.
- `ALT_LEFT` key switches view mode between all layers and single layer view. `ALT_RIGHT` key switches view mode between distinct (coloured) layers and sum of layers (white). The latter mode is used to change parameters in all layers at once.
- `BACKSPACE` key resets the axes.

### Parameter controls (bottom right sliders and menus)
  - Numerical space-time parameters *dt,dx* (smaller is more continuous) + adjustable *radius* of the mouse interaction (seeding, transfer, pixels lookup) and initialization (↺) spot.
  - Menu 1: Shape of the kernel (same for all neighbour fields).
  - Menu 2: Visualized field.
  - Menu 3: Parameter slider actions.
    - "Rule parameters" directly control parameters
    - "Mutation jumps" sets the average percentage change for each parameter if evolution is activated.
    - "Spatial gradient" sets a percentage of spatial gradient along the horizontal direction.
  - Intra-layer parameters:
    - *λ*: ratio of the spatial scales for the two neighbour fields. No gradient supported, set globally for all layers (for now).
    - $M_c, \Delta M_c, N_0, \Delta N_0, N_1, \Delta N_1$: parameters of the target function can be changed layer by layer, unless the layer sum mode is activated with `ALT_RIGHT` (then a change affects all layers).
  - Inter-layer parameters: the first 2 are the most important, the last 2 are related to evolution.
    - *λ₂ = 1* or *λ* sets the scale of the *Pₗ* weight fields.
    - $\alpha = 0, 1, 2, \infty$ sets the exponent of the *Pₗ* weight fields.
    - *τ₂* allows to test a non-instantaneous relaxation dynamics for the weight fields *Pₗ* when *τ₂≥dt*.
    - *ϵ* sets numerically undefined weights to equipartition if non-zero (zero otherwise).
    - *m₋* is the minimum mass in a layer to consider it as active.
    - *m₊* is the layer mass threshold beyond which the automatic transfer of cells in new layers is activated.
    - *ξ* is an average percentage of mutation noise (parameter jump), set in all layers at every jump.

### Summary of keyboard controls

Mouse pointing + `CTRL` or `SHIFT` to seed or transfer mass in current layer, `UP`/`DOWN` or `LEFT`/`RIGHT` to change current layer, `ALT_LEFT` and `ALT_RIGHT` to switch between layer view modes, `CTRL`+`X` or `Y` to restrict zoom (scroll) and pan (right-click), `BACKSPACE` (or `CTRL`+click) to reset images' zoom, `CTRL`+◯ for recording only the simulation, `SPACE`=▶️/⏯️, `ENTER`=↺ and `CTRL`+`C` to quit.

### How to activate evolution? (experimental)

1. Set the mutation noise *ξ* to a positive value (such as 0.01) to activate mutations at every transfer.
2. Set the mass threshold *m₊* to a finite value to activate auto-transfer.

A value of *m₊* around 7-11 can be used to keep only one cell per layer.

<!--
## cGoL

This scripts runs without GLMakie with a single layer. The first function `cGoL(dim)` contains the simulation loop and the different parameters.
-->

## How to contribute?

Any kind of input is welcome. You may also:
- check the TODO list in the header of the script,
- review the code (design can definitely be clarified, performance may be improved --> run on GPU?),
- review the model,
- join the dedicated Matrix chat room #cgol:matrix.org or contact me directly @a.guillet:matrix.org .

## Reference

### Please cite:
*Paper in preparation*

... so cite this repository in the meantime ;-)

### Early work (2016-2017)

https://yewtu.be/watch?v=3hZNAbmv6_k

### This simulation is based on:

* Cornus Ammonis' *Smoother Life*:
https://www.shadertoy.com/view/XtVXzV

...itself improving:

* Stephan Rafler's *SmoothLife*: https://arxiv.org/abs/1111.1567

Also have a look at the other variation of the model:

* Bert Chan's *Lenia*: https://doi.org/10/ggf344

### Licence

**GPL-v3**

Play with it as much as you like and contribute back your findings and questions to push the research forward!

### Funding

This work is funded by the **Max Planck Institute for the Physics of Complex Systems**, Dresden, Germany.
<img src="https://kb.pks.mpg.de/uploads/images/system/2022-09/image-1652963857399.png" width=500>