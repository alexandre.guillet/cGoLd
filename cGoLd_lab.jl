# cGoLd: continuous Game of Life, in arbitrary dimension with diversity

## Contact: a.guillet@ik.me

#= TODO:
## Model
* Mass-dependent rate of jump/mutation instead of a fixed threshold
* Mutation not at every jump?
* Preset for the average percentage of mutation depending on the parameter?
* Prevent oscillations close to the uniform fixed point? --> not easy
* Extend to different λ per layer (or even different τ?)
    --> requires an array of mconv/nconv functions

## Interface & Code
* Bug: parameters slider values are not updated when changed by a mutation in current layer (problem of scope of the L_jump!() function). Fix: change out and back layer by hand.
* Improve code design
    - best way to manage empty layers? scale to a large number?
        --> no performance improvement of using @views with [fill(:,dim)...,active] (but nicer functions)
    - improve scopes?
    - use struct? eg. for G, L, M, N, P
* enhance higher dimensions:
    - implement coloured visualization of layers for dim>=3
    - or select between 2D (image) and 3D (volume) modes for dim>=3
    - implement dim>3 with slider(?) selection of fixed coordinates
    - permutation (rotation) of axes for dim>3?
* add more presets in a new menu (autoload according to dim)
* improve the colouring, especially in draw_target (max value instead of sum)
* RESET
    - improve function / var init?
    - auto-reset at game over?
    - best init? global pattern and asymmetry control
* RECORD
    - correct screen size and options? 
    --> fix: resize by hand before recording OR GLMakie.activate!(decorated=false)
* SAVE button for saving interesting parameters?

## Optimization:
* mconv / nconv allocation?
* DONE (but unsure if it's the right thing) remove O (only P) in instantaneous regime? --> then O should also be replaced in L_jump!()
* CircularBuffer form DataStructures for the mass plot 
    --> more efficient? (downside: new dependency)
* inactivate layers in the regime dt₂<=1?
    - instantaneous vanishing of O but not of P --> undefined zones if P reach 0 (incorrect)
    --> replace by equipartition? require second array active2 and check...
    --> temporary solution: no inactivation

## Bugs
* "tightlimits!(ax)" for volume? / observable ranges? --> CLARIFY
* Working:▶️◀️⏯️⏪⏩⏮️⏭️⏫⏬⏏️ / Not working: ⏸⏹️⏺️🔼🔽
* mass x=fill(Observable(zeros(Float32,n1)),n2) does not behave as x=[Observable(zeros(Float32,n1))]; [push!(x, Observable(zeros(Float32,n1))) for _ in 2:n2]

Note: string/number input is possible --> reset with another dim param?
=#

using FFTW, Images, GLMakie, VideoIO
FFTW.set_num_threads(Threads.nthreads()÷2)
GLMakie.activate!(title = "cGoLd Lab", fullscreen = true) #, decorated=false)

## Simulation parameters
dim=2      # >=1
nlayer=2^4 # >=1

recordtitle="cGoLd_$dim-$nlayer.mkv" # default title for recorded video
recordtitle="~/Codes/Projects/Continuous_Game-of-Life/cGoL_Julia/cGoL_recordings/"*recordtitle # append path

function target(M,N,p) # p is simply a vector (rule)
    sigmoid(x, a, l) = l==0 ? x<a : 1/(1+exp((x-a)/l*4.4f0)) 
    gauss(x, a, l) = l==0 ? x==a : exp(-Float32(π)*((x-a)/l)^2)

    μ, σ = p[5].+(p[3]-p[5])*sigmoid.(M,p[1],p[2]), p[6].+(p[4]-p[6])*sigmoid.(M,p[1],p[2])
    return gauss.(N,μ,σ)
end
function target(M,N,param,grad) # param and grad are simply vectors (rule1)
    if sum(abs.(grad))==0
        return target(M,N,param)
    else
        sigmoid(x, a, l) = l==0 ? x.<a : 1 ./(1 .+exp.((x.-a)./l*4.4f0)) 
        gauss(x, a, l) = l==0 ? x==a : exp(-Float32(π)*((x-a)/l)^2)
    

        p=param.*(1 .-grad) #exp.(-grad)
        q=param.*(1 .+grad)

        lx=size(M,1)
        x=(0:lx-1)/(lx-1)
        pq=q'.*x .+p'.*(1 .-x)
        μ, σ = pq[:,5].+(pq[:,3].-pq[:,5]).*sigmoid.(M,pq[:,1],pq[:,2]), pq[:,6].+(pq[:,4].-pq[:,6]).*sigmoid.(M,pq[:,1],pq[:,2])

        return gauss.(N,μ,σ)
    end
end

function get_conv(n,nx,λ,chemical=false,patient=false)   # precompute the 2 convolutions
    if patient
        pr=plan_rfft(randn(Float32,n...); flags=FFTW.PATIENT,timelimit=Inf) # very slow to optimise a bit more
    else
        pr=plan_rfft(randn(Float32,n...); flags=FFTW.MEASURE,timelimit=Inf)
    end
    ipr=inv(pr)

    if chemical  # Cauchy filter / Bessel kernel as in a specific reaction-diffusion with source
        nr=copy(n); nr[1]=n[1]÷2+1; # for rfft

        rvec=FFTW.rfftfreq(n[1], nx).^2
        vec=zeros(nr...); vec.+=rvec
        if length(n)>1
            for l=eachindex(n[2:end])
                m=ones(Int,length(n)); m[l+1]=nr[l+1]
                vec.+=reshape(FFTW.fftfreq(n[l+1], nx).^2,m...)
            end
        end
        mfilter=1 ./(1 .+π*vec)
        nfilter=1 ./(1 .+π*vec*λ^2)
    else        # Gaussian filter / kernel
        mkernel=ones(n...)
        nkernel=ones(n...)
        for l=eachindex(n)
            m=ones(Int,length(n)); m[l]=n[l]
            mkernel.*=exp.(-π*reshape(FFTW.fftfreq(n[l], n[l]/nx),m...).^2)
            nkernel.*=exp.(-π*(reshape(FFTW.fftfreq(n[l], n[l]/(nx*λ)),m...)).^2)
        end
        mkernel/=sum(mkernel) 
        nkernel/=sum(nkernel)

        mfilter=real(pr*mkernel)
        nfilter=real(pr*nkernel)
    end
    
    mconv(L) = abs.(ipr*(mfilter.*(pr*L))) # what is the slow down due to abs.()? <-- not much
    nconv(L) = abs.(ipr*(nfilter.*(pr*L))) # abs.() is currently useful only for colouring purpose

    return mconv, nconv # mnconv(L)=map(filt->abs.(ipr*(filt.*(pr*L))),(mfilter,nfilter))
end

# *Add Gaussian spot to image*
function spot0(img,radius,cpos::CartesianIndex,dx=1) 
    n=size(img)
    dim=length(n)
    pos=zeros(dim)
    [pos[k]=(cpos[k]-1)*dx for k=1:dim]
    if length(radius)!=dim
        radius=ones(dim)*radius[1]
    end
    gausspot=zeros(n)
    x=(0:n[1]-1)*dx
    gausspot.+=min.((x.-pos[1]).^2,(x.-x[end].-x[2].-pos[1]).^2,(x.+x[end].+x[2].-pos[1]).^2)/radius[1]^2
    for k=2:length(n)
        x=(0:n[k]-1)*dx
        gausspot.+=reshape(min.((x.-pos[k]).^2,(x.-x[end].-x[2].-pos[k]).^2,(x.+x[end].+x[2].-pos[k]).^2)/radius[k]^2,fill(1,k-1)...,n[k])
    end
    gausspot=exp.(-Float32(π).*gausspot)
    return img = max.(img,gausspot)
end
function spot0(img,radius,pos,dx=1)
    n=size(img)
    dim=length(n)
    if length(radius)!=dim
        radius=ones(dim)*radius[1]
    end
    gausspot=zeros(n)
    x=(0:n[1]-1)*dx
    gausspot.+=min.((x.-pos[1]).^2,(x.-x[end].-x[2].-pos[1]).^2,(x.+x[end].+x[2].-pos[1]).^2)/radius[1]^2
    for k=2:length(n)
        x=(0:n[k]-1)*dx
        gausspot.+=reshape(min.((x.-pos[k]).^2,(x.-x[end].-x[2].-pos[k]).^2,(x.+x[end].+x[2].-pos[k]).^2)/radius[k]^2,fill(1,k-1)...,n[k])
    end
    gausspot=exp.(-Float32(π).*gausspot)
    return img = max.(img,gausspot)
end

function L_update!(G,L,M,N,P,mconv,nconv,param,grad,dt,Λ₂,dt₂,α,ϵ)
    dim=length(size(L))-1
    sL=dropdims(sum(L,dims=dim+1), dims=dim+1)
    M[:], N[:] = mconv(sL), nconv(sL)
    pconv = (Λ₂==1 ? mconv : nconv) # select scale for weight computation
    (dt₂>1 || ϵ==0) || (P0=copy(P))
    i=fill(:,dim)
    Threads.@threads for l in eachindex(param[1,:])
        (α>=0) && (P[i...,l]=pconv(L[i...,l]))
        G[i...,l]=target(M,N,param[:,l],grad[:,l])
    end

    (α>10) | (α==1) && (P[P.<=2*eps(1.f0)].=0) # visual cleanup
    if α==0
        P.=1/size(P)[end]
    elseif α>10 # α==Inf --> maximum value
        P.=(P.>=maximum(P,dims=dim+1)) .& (maximum(P,dims=dim+1).>0)
    elseif α>1
        [P.*=P for _=2:α]
    end
    
    if α>0 && (dt₂>1 || ϵ==0) # easy way to implement instantaneous relaxation
        ep = (ϵ>0 ? Float32(ϵ) : eps(1.f0))
        (ϵ>0 || dt₂<=1) && (P.=max.(P,ep)) # solves the pathological case dt₂<=1 && ϵ==0 (before setting ϵ[]=eps)
        # P[:]=P./sum(P,dims=dim+1)
        # (ϵ==0) && replace!(P,NaN=>0)
        if ϵ>0
            P[:]=P./sum(P,dims=dim+1) ### heavy (11%)
        else
            P[:]=P./max.(sum(P,dims=dim+1),ep)
            #P[:]=P./replace(sum(P,dims=dim+1),0=>ep)
        end
    elseif dt₂<=1 && ϵ>0
        P[:]=P0+(P.-sum(P,dims=dim+1).*P0)*dt₂ # TODO: how to inactivate layer?
    end
    L.+=(G.*P-L)*dt
end

function L_jump!(L,O,Lseed,Ljump,ilayer,active,pos,dx,radius,critical_mass,param,mutate,evo;colourmix=1:nlayer)
    szL=size(L); dim=length(szL)-1; nlayer=szL[end]
    i=fill(:,dim)
    ## Automatic dispatch of new cells into empty layers
    if sum(critical_mass)>0 
        layers=(1:nlayer)[critical_mass] # mother layers
        _, max_index = findmax(O.*L[i...,layers],dims=1:dim) # only get one max
        # _, max_index = findmax(O[i...,layers],dims=1:dim) # only get one max
        emptylayers=(1:nlayer)[.!active]
        emptylayers=filter(x -> x in emptylayers,colourmix) # mix
        for l = eachindex(layers) # assign empty layers if available, otherwise 1st layer
            dl = (l<=length(emptylayers) ? emptylayers[l] : (layers[l]==1 ? continue : 1)) # daughter layer
            spot=spot0(zeros(size(L[i...,1])),radius,max_index[l],dx)
            L[i...,dl].+=spot.*L[i...,layers[l]] # daughter
            L[i...,layers[l]].*=(1 .- spot)      # mother
            active[dl] || (active[dl]=true; evo && (param[:,dl]=param[:,layers[l]].*exp.(randn(7).*mutate[:,layers[l]]))) # activate if necessary and mutate if evolution
        end # TODO: what if several mother layers?
    end
    if dim<=2 && (Ljump | Lseed) && pos[1]>=0 && pos[2]>=0    # mouse action
        if Lseed # seeding
            L[i...,ilayer]=spot0(L[i...,ilayer],radius,pos,dx)
        else # Ljump # transfer
            spot=spot0(zeros(size(L[i...,1])),radius,pos,dx)
            indpos=Int.(round.(pos[1:dim]/dx.+1))
            ml=(L[indpos...,:].>0)[:] .& active # mother layers (select only relevant ones for speed)
            # ml=(O[indpos...,:].>0)[:] .& active # mother layers (select only relevant ones for speed)
            ml[ilayer]=false # no need to transfer from current layer
            L[i...,ilayer].+=spot.*sum(L[i...,ml],dims=dim+1)#[i...,1] # daughter
            L[i...,ml].*=(1 .- spot)                # mothers
            Ljump=false
        end # TODO: daughter inheritance of (main!) mother
        active[ilayer] || (active[ilayer]=true; evo && (param[:,ilayer].*=exp.(randn(7).*mutate[:,ilayer]))) # activate if necessary and mutate if evolution
    end
end

function colouring(s,G,L,M,N,P,ilayer,active,layermode; spot=([],2), offset=-60, param=[], recording=(false, false, false, nothing, nothing)) # TODO: colours in superimposed view?
    szP=size(P); dim=length(szP)-1; nlayer=szP[end]; i=fill(:,dim)
    img = s==1 ? G.*P-L.+1/nlayer : s==2 ? G.*P : s==3 ? L : s==4 ? M : s==5 ? N : P
    if dim<3
        if length(size(img))>dim 
            if layermode==1
                hue=360*(ilayer-1)/nlayer+offset
                val=img[i...,ilayer]
                img=RGB.(HSV.(hue, 1, val))
            elseif layermode==-1
                img=RGB.(dropdims(sum(img[i...,active],dims=dim+1), dims=dim+1))
            else
                layers=(1:nlayer)[active]
                val=img[i...,layers[1]]
                hue=360*(layers[1]-1)/nlayer+offset
                img0=RGB.(HSV.(hue, 1, val))
                for l=layers[2:end]
                    hue=360*(l-1)/nlayer+offset
                    val=img[i...,l]         ### heavy (6%)
                    img0.+=RGB.(HSV.(hue, 1, val))    ### heavy (23%)
                end
                img=copy(img0)
            end
            s==7 && (img.+=RGB(0.5,0.5,0.5))
        else
            img=RGB.(img)
        end
        ## show corresponding scatter <--> pixels in the simulation  # TODO: dim>=3?
        pos, radius=spot
        if !isempty(pos) && pos[1]>=-0.1 && pos[2]>=-0.1 && pos[1]<=1.1 && pos[2]<=1.1
            rL=exp.(-((N.-pos[1]).^2 .+(M.-pos[2]).^2)/(0.02*radius)^2)
            if layermode>=0
                img=RGB.(max.(rL,red.(img)),max.(rL,green.(img)),max.(rL,blue.(img)))
            else
                img=RGB.(HSV.(offset,rL,max.(rL,red.(img))))
            end
        end
    else # dim>=3
        if length(size(img))>dim
            if layermode<=0
                img=sum(img[i...,active],dims=dim+1)[:,:,:,fill(1+end÷2,dim-3)...,1] # for now, just plot the sum
            else
                img=img[:,:,:,fill(1+end÷2,dim-3)...,ilayer]
            end
        else
            img=img[:,:,:,fill(1+end÷2,dim-3)...]
        end
    end
    if recording[1][]
        recording[2] && try; recordframe!(recording[4]); catch; end # record scene
        recording[3] && write(recording[5], RGB{N0f8}.(min.(red.(img),1),min.(green.(img),1),min.(blue.(img),1))') # enforce value below 1 and convert to N0f8 to record frame
    end
    return img
end

function draw_target(param, ilayer, active, layermode; res=(400,400), offset=-60) 
    nlayer = size(param,2)
    if layermode==1 || nlayer==1
        val=target((0:res[1])/res[1],(0:res[2])'/res[2],param[:,ilayer])'
        img=RGB.(val)
#    elseif layermode==-1
#        img=RGB.(dropdims(sum([...],dims=dim+1), dims=dim+1))
    else
        layers=(1:nlayer)[active]
        sat=sum(active)/nlayer
        val=target((0:res[1])/res[1],(0:res[2])'/res[2],param[:,layers[1]])'
        hue=360*(layers[1]-1)/nlayer+offset
        img=RGB.(HSV.(hue, sat, val))
        for l=layers[2:end]
            val=target((0:res[1])/res[1],(0:res[2])'/res[2],param[:,l])'
            hue=360*(l-1)/nlayer+offset
            img.+=RGB.(HSV.(hue, sat, val))
        end
        img.*=(1/sum(active)+1/nlayer)
    end            
    return img
end

function cGoLd(dim::Int64, nlayer::Int64; savepath="cGoLd.mkv", framerate=24, compression=28, imsize=Int64[], screensize=(1920,1080), targetsize=(401,401), buffersize=2^10, backgroundcolor=Gray(0.3), colourangle=-60, nexc=5)
# Preset of image parameters
if isempty(imsize) # size preset if not provided
    dim==3 ? n=[1 1 1]*2^7 : (dim==2 ? n=[2 1]*2^8 : n=[1]*2^14)
    dim>=4 && (n=ones(Int64,1,dim)*2^6)
else
    n=imsize
end

# Trivial cases
nlayer*prod(imsize)==0 && return println("Void")
dim==0 && return println("Stable fixed points are typically near 0 and N₀+ΔN₀/2.")

nt=5 # preset resolution in frame per time unit
ldxmin=-float32(log2(minimum(n)/3)); nx=2 .^(-ldxmin)*dim/12 # preset resolution in pixel per physical unit
CHEMICAL=false; mconv, nconv=get_conv(n,nx,3,CHEMICAL) # Gaussian or Bessel
# λ₂=2; _, pconv=get_conv(n,nx,λ₂,CHEMICAL); # --> fixed to 1 or λ for speed

asym=2 .^((dim-1:-1:0)/3) # asymmetry of the RESET seed ################# undocumented / uncontroled

L=zeros(Float32,n...,nlayer) # O=copy(L); 
G=copy(L); P=zeros(Float32,n...,nlayer); N=copy(L[fill(:,dim)...,1]); M=copy(N)   # is it really necessary?
active=falses(nlayer); prev_active=copy(active)
xrange=Observable((0,(n[1]-1)/nx))
dim>=2 && (yrange=Observable((0,(n[2]-1)/nx)))
dim>=3 && (zrange=Observable((0,(n[3]-1)/nx)))

temps=Observable(zeros(Float32,buffersize))
mass=[Observable(zeros(Float32,buffersize))]
[push!(mass, Observable(zeros(Float32,buffersize))) for _ in 2:nlayer]
#mass=fill(Observable(zeros(Float32,buffersize)),nlayer) # does not work: WHY?

Lj=Observable(RGB.(zeros(targetsize...)))
if dim==1
    Li=Observable(RGB.([L[:,1] L[:,1]]))     # displayed as an image
elseif dim==2
    Li=Observable(RGB.(L[:,:,1]))
else
    Li=Observable(L[:,:,:,fill(1,dim-2)...]) # how to adapt RGB to volume plot?
end
Nj=Observable(N[1:nexc^dim:end])
Mj=copy(Nj)
Cj=Observable(RGB.(Nj[]))

# Figure
fig = GLMakie.Figure(backgroundcolor = backgroundcolor, size=screensize) #, size=(1920*4÷5,1080*4÷5)) # (width * (sqrt(5) / 2 - 1 / 2), ... , fonts = (; regular = "DejaVu")

# grid / panels
xsub=3:11
xsub2=1:xsub[1]-1

# Axis A: simulation in space (fields)
if dim==1
    ax, _=image(fig[1,xsub][1,1],xrange,(0,1),Li; colorrange = (0, 1), axis = (; backgroundcolor=backgroundcolor/2))
    hideydecorations!(ax)
elseif dim==2
    yrange=Observable((0,(n[2]-1)/nx))
    ax, _=image(fig[1,xsub][1,1],xrange,yrange,Li; colorrange = (0, 1), axis = (aspect = DataAspect(), tellheight=true, tellwidth=true, backgroundcolor=backgroundcolor/2))
elseif dim>=3
    ax = Axis3(fig[1,xsub][1,1], aspect=:data, perspectiveness=0.4) #, viewmode=:fit)
    volume!(ax, Li, colorrange = (0, 1), algorithm = :mip) #, transparency=true)#,colormap=:turbo)#,colormap=:grays)
    #hidedecorations!(ax)
    hidespines!(ax)
    ax.xlabel=""; ax.ylabel=""; ax.zlabel=""
end
# dim<=2 && deactivate_interaction!(ax, :rectanglezoom)  # disable zoom in axis ax (simulation)

# Axis B: simulation in target space G(N, O)
bx, _=image(fig[1,xsub2][1,1],(0, 1),(0, 1),Lj, axis = (; backgroundcolor=backgroundcolor/2)) #; axis = (; aspect = DataAspect(),)) #; colorrange = (0, 1))  # image
scatter!(bx,Nj,Mj, markersize=5, alpha=0.2, color=Cj)
bx.xlabel="Target function G(M,N)"
opt=(res=targetsize.-1, offset=colourangle) # drawing options

# Axis C: temporal analysis (mass)
cx=GLMakie.Axis(fig[1,xsub2][2,1], backgroundcolor=backgroundcolor/2) 
band!(cx,temps, zeros(Float32,buffersize), mass[1],color=HSV(360+colourangle, 1, 1), alpha=0.5, backlight=1.f0)
[band!(cx,temps, mass[l-1], mass[l],color=HSV(360*(l-1)/nlayer+colourangle, 1, 1),alpha=0.5, backlight=1.f0) for l=2:nlayer]
cx.title="Mass"

# Buttons
#fig[1,xsub2][3, 1][1,1]
fig[1,xsub2][4,1] = buttongrid = GridLayout(tellwidth = false)
PAUSE, RESET, RUN, RECORD = Observable(false), Observable(true), Observable(true), Observable(false)
btn = buttongrid[1,1:3] = [
Button(fig, label = @lift($PAUSE ? "▶️" : "⏯️")), 
Button(fig, label = "↺"), #↺↻
Button(fig, label = @lift($RECORD ? "⬤" : "◯"))]
for (ia,action) in enumerate([PAUSE, RESET, RECORD]) 
    on(btn[ia].clicks) do _; action[]=!action[]; end
end

# Sliders
sl1 = SliderGrid(fig[1,xsub][2,1][1,1][1,1],
(label="dt", range=2 .^(-7:0.1f0:0), startvalue = 1.f0/nt),
(label="dx", range=2 .^(ldxmin:0.02f0:0), startvalue = 1.f0/nx, color_inactive=backgroundcolor/2),
(label="radius", range=2 .^(-3:0.1f0:3), startvalue = 2, color_inactive=backgroundcolor/2)
) # numerical parameters
dt, dx, radius = [sl1.sliders[v].value for v=1:3]

## target parameters
par = [0.55 0.5 0.38; 0.06 0.11 0.1; 0.34 0.236 0.1; 0.038 0.015 0.004; 0.45 0.354 0.16; 0.34 0.236 0.11; 3 3 3]  # preset for target (direct)
par=Float32.(par[:,min(dim,3)])
prange = ([0.5f0; (0.8:0.001f0:1.2); 2]*par[1], [0; 2 .^(-3:0.01f0:3)*par[2]; 100], [0.001; 2 .^(-2:0.01f0:2)*par[3]; 1], [0; 2 .^(-2:0.01f0:2)*par[4]; 100], [0.002; 2 .^(-0.5f0:0.002f0:0.5f0)*par[5]; 2], [0; 2 .^(-2:0.01f0:2)*par[6]; 100], 2 .^(-1:0.01f0:1)*par[7])
symb=["Mᶜ"; "ΔMᶜ"; "N₀"; "ΔN₀"; "N₁"; "ΔN₁"; "λ"]

sl2 = SliderGrid(fig[1,xsub][2,1][1,2:3], tellheight = false,
[(label=symb[ip], range=prange[ip], startvalue = par[ip]) for ip=[7; 1:6]]...)
pr = circshift([sl2.sliders[v].value for v=1:7],-1)
λ=Observable(3.)

## layer dynamics parameters
sl4 = SliderGrid(fig[1,xsub][2,1][1,4], #valign=:bottom, #tellheight = false,
(label="λ₂", range=[1;3], startvalue = 1), # fixed to 1
(label="α", range=[0:3; 1000], startvalue = 2),
(label="ϵ", range=[0; eps(1.f0); 10.f0 .^(-6:-1)], startvalue = 0),
(label="τ₂", range=[0; 2 .^(-5:0.1:5)], startvalue = 0),
(label="m₋", range=[0; eps(1.f0); 10.f0 .^(-6:-1)], startvalue = 0.001),
(label="m₊", range=[0; 2.f0 .^(-3:0.02f0:10); 1e9], startvalue = 1e9),
(label="ξ", range=0:0.001f0:0.2f0, startvalue = 0),
)
Λ₂, α, ϵ, τ₂, m₋, m₊, ξ = [sl4.sliders[v].value for v in 1:7]

layerlabel = Observable("Current layer 1/$nlayer ($(sum(active)) active)")
Label(fig[1,xsub2][3,1], layerlabel, valign=:bottom, tellwidth=false)#, padding=(0, 0, -40, 0))

## theme
for sl in [sl1.sliders ; sl2.sliders ; sl4.sliders]
    sl.color_inactive=backgroundcolor/2
    sl.color_active_dimmed=backgroundcolor/2
    #sl.color_active=backgroundcolor*2
end

# Textbox(fig[1,xsub][2,1][1,5], placeholder = "$nlayer layers", tellwidth=false, textcolor_placeholder=RGBf(0,0,0)) # https://docs.makie.org/stable/reference/blocks/textbox/#tellwidth

# Menus
menu = Menu(fig[1,xsub][2,1][1,1][4,1], options = zip(["Gaussian kernel", "Bessel kernel"],[CHEMICAL, !CHEMICAL]), selection_cell_color_inactive=backgroundcolor*2) # (~exponential): diffusion steady state
kernel=menu.selection
menu2 = Menu(fig[1,xsub][2,1][1,1][3,1], selection_cell_color_inactive=backgroundcolor*2, options = zip(["G*P-L: rate of change","G*P: target","L: life", "M: cell", "N: neighbourhood","P: weight"], 1:6), default=3) 
imind=menu2.selection
menu3 = Menu(fig[1,xsub][2,1][1,1][2,1], selection_cell_color_inactive=backgroundcolor*2, options = zip(["Rule parameters","Mutation jumps","Spatial gradients"], 1:3), default=1) 
mode=menu3.selection

on(mode) do m
    m==1 ? layermode=0  : layermode=-1 # mutation and gradient are mostly global, so change of layermode
    lock=true
    for ip=1:6
        if m==1
            sl2.sliders[ip+1].range.val=prange[ip]      # .val not to trigger parameter change
            sl2.sliders[ip+1].startvalue.val=par[ip] 
            set_close_to!(sl2.sliders[ip+1], param[ip,ilayer])
        elseif m == 2
            sl2.sliders[ip+1].range.val=0:0.001f0:0.2f0   # .val not to trigger parameter change
            sl2.sliders[ip+1].startvalue.val=ξ[]
            set_close_to!(sl2.sliders[ip+1], mutate[ip,ilayer])
        else
            sl2.sliders[ip+1].range.val=-1:0.01f0:1
            sl2.sliders[ip+1].startvalue.val=0
            set_close_to!(sl2.sliders[ip+1], grad[ip,ilayer])
        end
    end
    if m==1
        sl2.sliders[1].range.val=prange[7]      # .val not to trigger parameter change
        sl2.sliders[1].startvalue.val=par[7] 
        set_close_to!(sl2.sliders[1], param[7,ilayer])
    else
        sl2.sliders[1].range.val=0:0      # .val not to trigger parameter change
        sl2.sliders[1].startvalue.val=0 
        set_close_to!(sl2.sliders[1], 0)
    end
    lock=false
end

on(kernel) do k
    mconv, nconv=get_conv(n,1/dx[],λ[],k)
end
on(pr[7]) do l
    if mode[]==1
        λ[]=l
        mconv, nconv=get_conv(n,1/dx[],l,kernel[])
        sl4.sliders[1].range=[1;l]
    end
end
on(dx) do d
    mconv, nconv=get_conv(n,1/d,λ[],kernel[])
    xrange[] = (0, (n[1]-1)*d)
    dim>=2 && (yrange[] = (0, (n[2]-1)*d))
    dim>=3 && (zrange[] = (0, (n[3]-1)*d))
    dim<=2 && tightlimits!(ax)
end
dt₂ = @lift $dt / $τ₂

## update labels
get_label = r -> ["0","1","λ",string(round(r[end],digits=1))]
xt = @lift [0, 1, $λ, $xrange[end]]  
dim>=2 && (yt = @lift [0, 1, $λ, $yrange[end]])
if dim>=3
    zt = @lift [0, 1, $λ, $zrange[end]]
    ztl = @lift ($zt./$dx, get_label($zrange))
    ytl = @lift ($yt./$dx, get_label($yrange))
    xtl = @lift ($xt./$dx, get_label($xrange))
    on(ztl) do tl; ax.zticks = tl; end
else
    dim==2 && (ytl = @lift ($yt,get_label($yrange)))
    xtl = @lift ($xt, get_label($xrange))
end
on(xtl) do tl; ax.xticks = tl; end
dim>=2 && on(ytl) do tl; ax.yticks = tl; end

for ip=1:6  # update parameters[1:6] and gradient when slider changes # WARNING: triggered more often than necessary
    on(pr[ip]) do p # do only if not changing mode
        lock || if mode[]==1
            layermode>=0 ? param[ip,ilayer]=p : param[ip,:].=p
            refresh()
            bx.xticks = ([0,pr[3][],pr[5][],1], ["0","N₀","N₁","1"])
            bx.yticks = ([0,pr[1][],1], ["0","Mᶜ","1"])
        elseif mode[]==2
            layermode>=0 ? mutate[ip,ilayer]=p : mutate[ip,:].=p
        elseif mode[]==3
            layermode>=0 ? grad[ip,ilayer]=p : grad[ip,:].=p
        end
    end
end

on(ξ) do x # when changed, set mutation jump percentage globally(?)
    mutate[1:6] .= x 
    mode[]==2 && [set_close_to!(sl2.sliders[ip+1], x) for ip=1:6]
end

on(events(fig).keyboardbutton) do event # Keyboard actions
    if event.action==Keyboard.press  # if key is pressed
        event.key == Keyboard.space && (PAUSE[] = !PAUSE[])
        event.key == Keyboard.c && ispressed(ax,Keyboard.left_control) && (RUN[]=false) # GLMakie.closeall() <-- works but throw an error about tasks 
        event.key == Keyboard.enter && (RESET[] = true)
        event.key == Keyboard.backspace && (reset_limits!(ax); reset_limits!(bx))

        if event.key in (Keyboard.up, Keyboard.right, Keyboard.down, Keyboard.left)                 ## change current layer
            event.key == Keyboard.up && (ilayer=mod(ilayer+1,1:nlayer))
            event.key == Keyboard.down && (ilayer=mod(ilayer-1,1:nlayer))
            event.key == Keyboard.right && (ilayer=circshift(mix,-1)[findfirst(isequal(ilayer),mix)]) # change layer in mixed order
            event.key == Keyboard.left && (ilayer=circshift(mix,1)[findfirst(isequal(ilayer),mix)])

            val = (mode[]==1 ? param[:,ilayer] : mode[]==2 ? mutate[:,ilayer] : grad[:,ilayer])
            [isapprox(pr[ip][], val[ip]; rtol=0.005) || (lock=true; set_close_to!(sl2.sliders[ip+1], val[ip])) for ip=1:6]  # display param of the layer (only if different), lock prevents multiple observable updates
            lock ? (lock=false; notify(pr[1])) : refresh(layermode==1) # update now
        end

        if event.key in (Keyboard.left_alt, Keyboard.right_alt)   ## change layer mode
            if event.key==Keyboard.left_alt
                layermode!=1 ? layermode=1 : layermode=0
            else
                layermode!=-1 ? layermode=-1 : layermode=0
            end
            refresh()
        end
        ## activate mouse transfer towards current layer
        event.key in (Keyboard.left_shift, Keyboard.right_shift) && (Ljump=true)
        ## activate mouse seeding in current layer
        event.key in (Keyboard.left_control, Keyboard.right_control) && (Lseed=true)
    else # when key is released
        event.key in (Keyboard.left_shift, Keyboard.right_shift) && (Ljump=false)
        event.key in (Keyboard.left_control, Keyboard.right_control) && (Lseed=false)
    end
end
lock=false

sq = n -> 1 .+reduce(hcat,digits.(0:2^n-1,base=2,pad=n))'*2 .^(n-1:-1:0) # mix layer order
mix=filter(x -> x <= nlayer, sq(Int(ceil(log2(nlayer))))) # get mixed layer indices
i=fill(:,dim)

on(RECORD) do r
    if r
        if dim==2 && ispressed(ax,Keyboard.left_control)
            recordframe = true
            framewriter=open_video_out(replace(savepath,id=>string(time())[end-11:end-8]), convert.(RGB{N0f8},Li[]'), framerate=framerate, codec_name="libx265", encoder_options=(crf=compression, preset="medium")) # change ID
        else
            recordscene = true
            scenewriter=VideoStream(fig, framerate=framerate, visible=true, encoder="libx265", compression=compression) #, px_per_unit = 1, no preset option!
        end
    else
        recordscene && save(replace(savepath,id=>string(time())[end-11:end-8]),scenewriter) # change ID
        recordframe && close_video_out!(framewriter)
        recordscene, recordframe = false, false
    end
end
## append an ID to the record title
id="0000"
if savepath[end-3]=='.' # assume .mkv, .mp4, .gif ...
    savepath=savepath[1:end-4]*"_"*id*savepath[end-3:end]
else # let the writer decide
    savepath=savepath*"_"*id
end

function refresh(draw=true)
    @async draw && (Lj[]=draw_target(param, ilayer, active, layermode; opt...))
    @async layerlabel[] = "Current layer $ilayer/$nlayer ($(sum(active)) active)"
end

function reset!()
    active=falses(size(active)); active[ilayer]=true
    layermode = (nlayer>1 ? 0 : -1)

    P=zeros(Float32,size(P)); P[i...,ilayer].=1
    L=zeros(Float32,size(L)); M=zeros(Float32,n...); N=copy(M) #O=copy(L);
    L[i...,ilayer]=spot0(L[i...,ilayer],asym*radius[]/dx[],n/2)
    t = zero(dt[])

    # TODO: Carefully consider what to reset and what not...
    # set_close_to!(sl1.sliders[2],1/nx)
    #set_close_to!(sl2.sliders[1], par[7])
    #[set_close_to!(sl2.sliders[ip+1], par[ip]) for ip=1:6] 
    #[set_close_to!(sl3.sliders[ip+1], 0) for ip=1:6]
    refresh()
    RESET[]=!RESET[]
end

## Initialisations # TODO: what is not necessary? / in the wrong scope?
scenewriter, framewriter=nothing, nothing # for recording
recordscene, recordframe = false, false   # for recording
Ljump, Lseed = false, false # for mouse actions
param, mutate, grad = par[:]*ones(Float32,1,nlayer), zeros(Float32,7,nlayer), zeros(Float32,7,nlayer) # layer-wise parameters
ilayer, layermode = 1, (nlayer>1 ? 0 : -1)
date, t, tind, FPS = time(), zero(dt[]), 0, 10.
Mass=sum(L,dims=1:dim)[:]*dx[]^dim
notify(λ); notify(pr[1]) # draws tick labels

display(fig)
while RUN[] && isopen(fig.scene) # SIMULATION LOOP
    PAUSE[] && (sleep(0.1); continue)
    RESET[] && reset!() # || sum(Mass)<300*eps(1.f0)   

    ## Scatter in axis bx
    @async begin 
        if mod(tind,Int(ceil(FPS/5)))==0  # colour of scatter in axis bx
            if layermode>=0 
                tempP=reshape(P,:,nlayer)[1:nexc^dim:end,:]
                Cj[] = colouring(6,fill(nothing,4)...,reshape(tempP,:,fill(1,dim-1)...,nlayer),ilayer,active,layermode; offset=colourangle)[:] # tricks to get the right sizes / dimensions for each task
            else
                Cj[] = HSV.(colourangle,1,ones(length(Mj[])))
            end
        end
        Nj.val=N[1:nexc^dim:end]
        Mj[]=M[1:nexc^dim:end]
    end

    ## Compute mass per layer for plot cx
    @async begin
        Mass=sum(L,dims=1:dim)[:]*dx[]^dim  # heavy
        for l=1:nlayer
            if active[l] && dt₂[]>1 && sum(active)>1 && Mass[l]<=m₋[] # exclude regime 
                active[l]=false
                L[i...,l].=0; 
                P[i...,l].=0 # not necessary for computation but visible # O[i...,l].=0 <-- not necessary
                Mass[l]=0
            end
            circshift!(mass[l][],-1); mass[l][][end]=sum(Mass[1:l]) # mutates obervable without triggering listener (plot) # heavy
        end
        ## Update time
        temps[].-=dt[]; temps[][1]=0; circshift!(temps[],-1); # circular buffer # heavy
        tind += 1; mod(tind,30)==0 && ((date, FPS)=(time(), 30/(time()-date))) # estimate FPS
        t += dt[]; cx.xlabel="Time: t = $(round(t; digits=1)) s      ($(Int64(round(FPS))) fps)"
        mod(tind,Int(ceil(FPS/10)))==0 && (limits!(cx,temps[][1],0,0,eps(1.0f0)+maximum(mass[end][])*1.01f0); notify(temps)) # heavy

        prev_active==active || (prev_active=copy(active); refresh()) # refresh() is heavy if repeated
    end

    ################ Core ################
    L_jump!(L,M,Lseed,Ljump,ilayer,active,mouseposition(ax.scene),dx[],radius[],Mass.>m₊[],param,mutate,ξ[]>0;colourmix=mix) # Seed and transfer
    Li0=colouring(imind[],G,L,M,N,P,ilayer,active,layermode; recording=(RECORD, recordscene, recordframe, scenewriter, framewriter), spot=(mouseposition(bx.scene),radius[]), offset=colourangle); Li[] = (dim>=2 ? Li0 : [Li0 Li0])
    @views L_update!(G[i...,active],L[i...,active],M,N,P[i...,active],mconv,nconv,param[:,active],grad[:,active],dt[],Λ₂[],dt₂[],α[],ϵ[]) # Evolution
    ######################################

    # set ϵ>0 after switching from dt₂>1 to dt₂<=1 to ensure that all proba are defined (normalized), see update_P!() (not in its scope)
    @async dt₂[]<=1 && ϵ[]==0 && set_close_to!(sl4.sliders[3],eps(1.f0)) # A BIT HACKY (@async seems now necessary with @views...)
    
    dim>=3 ? ax.azimuth[] += 2π/360*dt[] : nothing
end # of simulation loop

# close gracefully
RECORD[]=false     # stop recording
GLMakie.closeall() # close figure
println("Game Over")
end # of function

cGoLd(dim, nlayer; savepath=recordtitle) # run the simulation



############################################### COMMENTS ###############################################
#=

# Previous method with the intermediary O field

function L_update!(G,L,M,N,O,P,mconv,nconv,param,grad,dt,Λ₂,dt₂,α,ϵ)
    dim=length(size(L))-1
    G_update!(G,L,M,N,O,mconv,nconv,param,grad,Λ₂)
    P_update!(P,O,dt₂,α,ϵ)
    L.+=(G.*P-L)*dt
end

function G_update!(G,L,M,N,O,mconv,nconv,param,grad,Λ₂)
    sL=dropdims(sum(L,dims=dim+1), dims=dim+1)
    M[:], N[:] = mconv(sL), nconv(sL)
    oconv = (Λ₂==1 ? mconv : nconv) # select scale for weight computation
    i=fill(:,dim)
    Threads.@threads for l in eachindex(param[1,:])
        O[i...,l]=oconv(L[i...,l])
        G[i...,l]=target(M,N,param[:,l],grad[:,l])
    end
end

function P_update!(P,O,dt₂,α,ϵ) # same without P jump (L jump instead)
    if α==0 # equipartition # TODO: reassign only if α or active have changed?
        P.=1/size(P)[end]
    ## Case α==Inf not handled here
    else
        [O.*=O for _=2:α] # O=O.^α is slow
        if dt₂>1 || ϵ==0 # easy way to implement instantaneous relaxation
            ep = (ϵ>0 ? Float32(ϵ) : eps(1.f0))
            (ϵ>0 || dt₂<=1) && (O=max.(O,ep)) # solves the pathological case dt₂<=1 && ϵ==0 (before setting ϵ[]=eps)
            norm=sum(O,dims=dim+1)
            ϵ==0 && (norm=max.(norm,ep)) # to avoid undefined ratios
            #foreach(normalize!, eachslice(O,dims=dim+1))
        # P[:]=O
        # P./=sum(P,dims=dim+1)
        # ϵ==0 && replace!(P, NaN=>0)
            #ϵ==0 && replace!(P, NaN=>0)
            #P[isnan.(P)].=0
            P[:]=O./norm ### heavy (11%)
        else
            dP=O.-sum(O,dims=dim+1).*P # TODO: how to inactivate layer?
            P.+=dP*dt₂
        end
    end
end





Copyright (C) 2024  Alexandre Guillet

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
=#
